﻿namespace VRTK
{

    using UnityEngine;
    using System.Collections;

    public class BallSpawner : MonoBehaviour
    {

        [SerializeField] private GameObject ballPrefab;
        private VRTK_InteractGrab m_Grab;
        private VRTK_InteractTouch m_Touch;


        private void Start()
        {
            if (GetComponent<VRTK_ControllerEvents>() == null)
            {
                Debug.LogError("VRTK_ControllerEvents_ListenerExample is required to be attached to a Controller that has the VRTK_ControllerEvents script attached to it");
                return;
            }

            if (GetComponent<VRTK_InteractGrab>() == null)
            {
                Debug.LogError("VRTK_InteractGrab is required to be attached to a Controller that has the VRTK_ControllerEvents script attached to it");
                return;
            }

            
            //Setup controller event listeners
            GetComponent<VRTK_ControllerEvents>().TriggerPressed += new ControllerInteractionEventHandler(DoTriggerPressed);

            m_Grab = GetComponent<VRTK_InteractGrab>();
            m_Touch = GetComponent<VRTK_InteractTouch>();
        }


        private void DoTriggerPressed(object sender, ControllerInteractionEventArgs e)
        {
            GameObject thisBall = Instantiate(ballPrefab);
            //Rigidbody thisBallRB = thisBall.GetComponent<Rigidbody>();

            thisBall.transform.position = this.transform.position;

            m_Touch.ForceTouch(thisBall);

            m_Grab.AttemptGrab();
            //m_Grab.ForceGrabObject(thisBall);


        }
 
    }

}