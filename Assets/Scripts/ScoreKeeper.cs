﻿using UnityEngine;
using System.Collections;

public class ScoreKeeper : MonoBehaviour {

    public int totalScore = 0;

    private AudioSource thisAudioSource;


    // Use this for initialization
    void Start ()
    {
        DontDestroyOnLoad(this.gameObject);

        thisAudioSource = GetComponent<AudioSource>();
    }
	


    public void UpdateScore(int scoreChange)
    {
        totalScore += scoreChange;

        thisAudioSource.Play();        
    }


    

}
