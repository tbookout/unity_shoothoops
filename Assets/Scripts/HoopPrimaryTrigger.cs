﻿using UnityEngine;
using System.Collections;

public class HoopPrimaryTrigger : MonoBehaviour {
    
    private HoopSecondaryTrigger myHoopSecondaryTrigger;


    // Use this for initialization
    void Start () {

        myHoopSecondaryTrigger = GetComponentInChildren<HoopSecondaryTrigger>();
    }
	

    void OnTriggerEnter(Collider thisCollider)
    {
        if (thisCollider.gameObject.tag == "ScoreBall")
        {
            myHoopSecondaryTrigger.ExpectCollider(thisCollider);
        }
    }

    
}
