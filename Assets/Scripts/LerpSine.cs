﻿using UnityEngine;
using System.Collections;

public class LerpSine : MonoBehaviour {

    public float amplitude = 4f;
    public float period = 1f;
    public int moveAxis = 0;
       
    private Vector3 startPos;
    private Vector3 moveDir;


    protected void Start()
    {
        startPos = transform.position;

        if (moveAxis == 1) { moveDir = Vector3.right; }
        else if (moveAxis == 2) { moveDir = Vector3.up; }
        else if (moveAxis == 3) { moveDir = Vector3.forward; }
        else { moveDir = Vector3.zero; }
    }


    protected void Update()
    {        
        float theta = Time.timeSinceLevelLoad / period;
        float distance = amplitude * Mathf.Sin(theta);
        transform.position = startPos + moveDir * distance;
    }


}
