﻿using UnityEngine;
using System.Collections;

public class CameraRotation : MonoBehaviour {

    Camera thisCamera;

    float xRotSpeed = 2.5f;
    float yRotSpeed = -2.5f;


    // Use this for initialization
    void Start () {
        thisCamera = GetComponentInChildren<Camera>();
    }
	
	// Update is called once per frame
	void Update () {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        thisCamera.transform.rotation *= Quaternion.Euler((mouseY * yRotSpeed), 0, 0);
        this.transform.rotation *= Quaternion.Euler(0, (mouseX * xRotSpeed), 0);

    }
}
