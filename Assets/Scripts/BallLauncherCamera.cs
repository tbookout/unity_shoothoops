﻿using UnityEngine;
using System.Collections;

public class BallLauncherCamera : MonoBehaviour {

    [SerializeField] private GameObject ballPrefab;
    [SerializeField] private float shotVelocity = 10f;

    private Camera myCamera;


    // Use this for initialization
    void Start()
    {
        myCamera = GetComponentInParent<Camera>();

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Fire1"))
        {
            ShootBall();
        }
        
    }


    private void ShootBall()
    {
        GameObject thisBall = Instantiate(ballPrefab);
        Rigidbody thisBallRB = thisBall.GetComponent<Rigidbody>();

        thisBall.transform.position = this.transform.position;
        thisBallRB.velocity = myCamera.transform.rotation * (new Vector3(0, 0.4f, 0.6f) * shotVelocity );
    }
    

}
