﻿using UnityEngine;
using System.Collections;

public class ScoreAbsorber : MonoBehaviour {

    public int scoreValue = 0;

	// Use this for initialization
	void Start ()
    {
        ScoreKeeper myScoreKeeper = GameObject.FindObjectOfType<ScoreKeeper>();

        if (myScoreKeeper)
        {
            scoreValue = myScoreKeeper.totalScore;
            Destroy(myScoreKeeper.gameObject);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
