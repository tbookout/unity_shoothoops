﻿using UnityEngine;
using System.Collections;

public class RotTest1 : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.rotation = transform.rotation * Quaternion.AngleAxis(5, Vector3.up);
            Debug.Log(transform.rotation);
        }
        else if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.rotation = Quaternion.AngleAxis(5, Vector3.up) * transform.rotation;
        }
        
    }
}
