﻿using UnityEngine;
using System.Collections;

public class BallLauncher : MonoBehaviour {

    [SerializeField] private GameObject ballPrefab;
    [SerializeField] private float shotVelocity = 6f;

    Camera myCamera;


    // Use this for initialization
    void Start()
    {
        myCamera = GetComponentInChildren<Camera>();

    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetButtonDown("Fire1"))
        {
            ShootBall();
        }


        //if (Input.GetKeyDown(KeyCode.LeftArrow))
        //{
        //    CreateBall(Vector3.left);
        //}
        //else if (Input.GetKeyDown(KeyCode.RightArrow))
        //{
        //    CreateBall(Vector3.right);
        //}
        //else if (Input.GetKeyDown(KeyCode.DownArrow))
        //{
        //    CreateBall(Vector3.back);
        //}
        //else if (Input.GetButtonDown("Fire1"))
        //{
        //    CreateBall(Vector3.forward);
        //}        
    }


    private void ShootBall()
    {
        GameObject thisBall = Instantiate(ballPrefab);
        Rigidbody thisBallRB = thisBall.GetComponent<Rigidbody>();

        thisBall.transform.position = this.transform.position;
        thisBallRB.velocity = myCamera.transform.rotation * (Vector3.forward * shotVelocity);
    }

}
