﻿using UnityEngine;
using System.Collections;


public class LerpExample : MonoBehaviour
{
    float lerpTime = 3f;  // # of seconds for object to move from start to finish
    float currentLerpTime = 0f;  // # of seconds that have passed

    float moveDistance = 10f;

    Vector3 startPos;
    Vector3 endPos;

    protected void Start()
    {
        startPos = transform.position;
        endPos = transform.position + Vector3.up * moveDistance;
    }

    protected void Update()
    {
        //reset when we press spacebar
        if (Input.GetKeyDown(KeyCode.Space))
        {
            currentLerpTime = 0f;
        }

        //increment timer once per frame
        currentLerpTime += Time.deltaTime;
        if (currentLerpTime > lerpTime)
        {
            currentLerpTime = lerpTime;
        }

        //lerp!
        float perc = currentLerpTime / lerpTime;
        //perc *= perc;  //exponential movement

        transform.position = Vector3.Lerp(startPos, endPos, perc);
        //transform.position = Vector3.Lerp(startPos, endPos, lerpTime);

    }
}