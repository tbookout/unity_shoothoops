﻿using UnityEngine;
using System.Collections;

public class LerpRotate : MonoBehaviour {

    float lerpTime = 2f;  // # of seconds for object to move from start to finish
    float currentLerpTime = 0f;  // # of seconds that have passed

    float rotationAngle = 180;  // Values greater then 180 behave as 360-180

    Quaternion startRot;
    Quaternion endRot;

    protected void Start()
    {
        startRot = transform.rotation;
        endRot = transform.rotation * Quaternion.Euler(0, rotationAngle, 0);
    }


    protected void Update()
    {
        //reset when we press spacebar
        if (Input.GetKeyDown(KeyCode.Space))
        {
            currentLerpTime = 0f;
        }

        //increment timer once per frame
        currentLerpTime += Time.deltaTime;
        if (currentLerpTime > lerpTime)
        {
            currentLerpTime = lerpTime;
        }

        //lerp!
        float perc = currentLerpTime / lerpTime;
        transform.rotation = Quaternion.Slerp(startRot, endRot, perc);

    }


}




