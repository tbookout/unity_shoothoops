﻿using UnityEngine;
using System.Collections;

public class ScoreTarget : MonoBehaviour {

    [SerializeField] private int scoreValue = 1;

    private ScoreKeeper myScoreKeeper;


    // Use this for initialization
    void Start ()
    {
        myScoreKeeper = FindObjectOfType<ScoreKeeper>();        
    }
	


    void OnCollisionEnter(Collision thisCollision)
    {

        if (thisCollision.gameObject.tag == "ScoreBall")
        {
            myScoreKeeper.UpdateScore(scoreValue);            
        }
    }

}
