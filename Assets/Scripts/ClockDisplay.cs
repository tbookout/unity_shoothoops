﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;


public class ClockDisplay : MonoBehaviour {

    private LevelManager myLevelManager;
    private Text clockValueTxt;


    // Use this for initialization
    void Start()
    {
        myLevelManager = GameObject.FindObjectOfType<LevelManager>();
        clockValueTxt = GetComponent<Text>();

    }

    // Update is called once per frame
    void Update()
    {
        clockValueTxt.text = myLevelManager.secondsTillLevelLoad.ToString("F");

    }
}
