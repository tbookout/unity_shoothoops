﻿using UnityEngine;
using System.Collections;

public class Shredder : MonoBehaviour {

    void OnTriggerEnter(Collider thisCollider)
    {
        Destroy(thisCollider.gameObject);        
    }
}
