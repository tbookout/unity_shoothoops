﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;


public class ScoreDisplay : MonoBehaviour {

    private ScoreKeeper myScoreKeeper;
    private Text scoreValueTxt;

    // Use this for initialization
    void Start ()
    {
        myScoreKeeper = GameObject.FindObjectOfType<ScoreKeeper>();
        scoreValueTxt = GetComponent<Text>();

    }
	
	// Update is called once per frame
	void Update ()
    {
        scoreValueTxt.text = myScoreKeeper.totalScore.ToString();

    }
}
