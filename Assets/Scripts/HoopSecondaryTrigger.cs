﻿using UnityEngine;
using System.Collections;

public class HoopSecondaryTrigger : MonoBehaviour {

    [SerializeField] private int scoreValue = 1;

    private ScoreKeeper myScoreKeeper;
    private Collider saveCollider;




    // Use this for initialization
    void Start () {
        myScoreKeeper = FindObjectOfType<ScoreKeeper>();        
    }
	
    
    public void ExpectCollider(Collider myCollider)
    {
        saveCollider = myCollider;
    }


    void OnTriggerEnter(Collider thisCollider)
    {       
        if (thisCollider == saveCollider)
        {           
            myScoreKeeper.UpdateScore(scoreValue);            
        }

        thisCollider.gameObject.tag = "DeadBall";
    }


}
