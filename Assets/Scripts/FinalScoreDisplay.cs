﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;


public class FinalScoreDisplay : MonoBehaviour {

    private ScoreAbsorber myScoreAbsorber;
    private Text scoreValueTxt;

    // Use this for initialization
    void Start ()
    {
        myScoreAbsorber = GameObject.FindObjectOfType<ScoreAbsorber>();
        scoreValueTxt = GetComponent<Text>();

    }
	
	// Update is called once per frame
	void Update ()
    {
        scoreValueTxt.text = myScoreAbsorber.scoreValue.ToString();

    }
}
