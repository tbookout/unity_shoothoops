﻿using UnityEngine;
using System.Collections;

public class BallBounceSound : MonoBehaviour {

    private AudioSource thisAudioSource;

    // Use this for initialization
    void Start () {

        thisAudioSource = GetComponent<AudioSource>();
        
    }


    void OnCollisionEnter()
    {
        thisAudioSource.Play();        
    }

}
